@extends('pie_base::admin.layouts.edit')

@section('title_name'){{trans('pie_article::main.sub_menu_news')}}@endsection

@section('head')
    <link rel="stylesheet" href="{{$app['pie_article.assets']->getPath('css/article-edit.css')}}" >
@endsection

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_info')) !!}
        {!! Form::text('info', $model->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_cover')) !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> {{trans('pie_article::main.select_cover')}}</div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
    </div>

    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_text')) !!}
        {!! Form::textarea('text', $model->text, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_category')) !!}
        {!! Form::select('category_id', $categories,$model->category_id, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_creator')) !!}
        {!! Form::select('user_id', $users, ($model->user_id) ? $model->user_id : Request::user()->id, ['class'=>'form-control']) !!}
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/themes/gray.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor_ie8.min.js')}}"></script>
    <![endif]-->
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/tables.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/lists.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/media_manager.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_family.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_size.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/block_styles.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/video.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').editable({
                inlineMode: false,
                theme: 'gray',
                height: '650',
                language: 'ru'
            })
            $('.froala-box div').last().remove()
        });

    </script>
@endsection