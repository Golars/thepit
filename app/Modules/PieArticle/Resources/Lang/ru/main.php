<?php
return [
    'menu_news' => 'Публикации',
    'sub_menu_news' => 'Новости',
    'sub_menu_category' => 'Категории',
    'table_info' => 'Информация',
    'table_cover' => 'Обложка',
    'table_owner' => 'Автор',
    'table_category' => 'Категория',
    'table_color' => 'Цвет',
    'table_text' => 'Контент',
    'table_creator' => 'Автор',
    'select_cover' => 'Выбрать обложку',
];