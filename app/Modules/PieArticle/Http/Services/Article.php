<?php
namespace App\Modules\PieArticle\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use App\Modules\PieConfig\Http\Services\Config;
use Illuminate\Http\Request;

class Article extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieArticle\Database\Models\Articles';

    public function oneArticle($id)
    {
        $model = $this->getModel();
        $this->setWhere(['status' => $model::STATUS_ACTIVE]);
        return $this->getOne($id);
    }

    public function prepareSelect()
    {
        $articles = [];
        foreach ($this->getAll() as $article) {
            $articles[$article->id] = $article->name;
        }

        return $articles;
    }

    private function articleFilter(Array $parameters)
    {
        $query = $this->getModel()->query()->active();
        $query = ($parameters['category']) ? $query->where(['category_id' => $parameters['category']]) : $query;

        if (isset($parameters['filter']) && $parameters['filter']) {
            $collection = $query->get();
            $ids = $collection->map(function ($model) {
                return $model->id;
            });
            if ($ids && !empty($ids)) {
                $translates = new App\Modules\PieTranslator\Http\Services\ArticleTranslator();
                $t_ids = $translates->getModel()->query()
                    ->whereIn('article_id', $ids)
                    ->where(function ($query) use ($parameters) {
                        return
                            $query->where('trans_name', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('trans_info', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('trans_text', 'LIKE', "%#{$parameters['filter']}%");
                    })
                    ->get()->map(function ($model) {
                        return $model->article_id;
                    })->toArray();
                if ($t_ids && !empty($t_ids)) {
                    $query = $query->where(function ($query) use ($t_ids, $parameters) {
                        return $query->whereIn('id', $t_ids)
                            ->orWhere(function ($query) use ($parameters) {
                                return
                                    $query->where('name', 'LIKE', "%{$parameters['filter']}%")
                                        ->orWhere('info', 'LIKE', "%{$parameters['filter']}%")
                                        ->orWhere('text', 'LIKE', "%#{$parameters['filter']}%");
                            });
                    });
                } else {
                    $query->where(function ($query) use ($parameters) {
                        return
                            $query->where('name', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('info', 'LIKE', "%{$parameters['filter']}%")
                                ->orWhere('text', 'LIKE', "%#{$parameters['filter']}%");
                    });
                }
            }
        }
        return $query;
    }

    public function paginateArticles(Request $request, $categoryName = null, $paginate = 12)
    {
        $parameters['category'] = $request->input('category');
        $parameters['filter'] = $request->input('filter');
        return $this->articleFilter($parameters)
            ->createdBy()
            ->paginate($paginate);
    }

    public function getPopular($take = 5)
    {
        return $this->getModel()->query()->orderBy('views', 'desc')->take($take)->get();
    }

    public function getDiscussed($take = 5)
    {
        return $this->getModel()->query()
            ->with('comments')
            ->get()
            ->sortBy(function($model)
            {
                return $model->comments->count();
            },SORT_REGULAR,true)->take($take);
    }

    public function filterArticles(Request $request)
    {
        $filter = $request->input('filter');
        $query = $this->getModel()->query()->active();
        return $query->where('name', 'LIKE', "%{$filter}%")
            ->orWhere('info', 'LIKE', "%{$filter}%")->createdBy()->paginate(12);
    }

    public function getActual($inModel, $countNews = 6)
    {
        $count = 0;
        $ids = [];
        $moreViewedInCategory = $this->getModel()
            ->where('category_id', $inModel->category_id)
            ->where('id', '!=', $inModel->id)
            ->orderBy('views', 'desc')->take($countNews % 4)->get();

        if (!empty($moreViewedInCategory)) {
            foreach ($moreViewedInCategory as $model) {
                $ids[] = $model->id;
            }
            $count = $count + $moreViewedInCategory->count();

            $newInCategory = $this->getModel()
                ->where('category_id', $inModel->category_id)
                ->where('id', '!=', $inModel->id)
                ->whereNotIn('id', $ids)
                ->orderBy('created_at', 'desc')->take($countNews % 2)->get();
            if ($newInCategory) {
                $count = $count + $newInCategory->count();
            }
        }
        $other = $this->getModel()
            ->where('id', '!=', $inModel->id);

        if (isset($newInCategory) && !empty($newInCategory)) {
            foreach ($newInCategory as $model) {
                $ids[] = $model->id;
            }
            $other = $other->whereNotIn('id', $ids);
        }
        $other = $other->orderBy('created_at', 'desc')->take($countNews - $count)->get();
        if ($other) {
            foreach ($other as $model) {
                $ids[] = $model->id;
            }
        }
        return $this->getModel()->whereIn('id', $ids)->get();
    }
}