<?php
namespace App\Modules\PieBase\Database\Models;

use App;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class Cover extends File
{
    protected $path = 'users/';

    private function prepareAvatar($w = null, $h = null)
    {
        $cropPath = $this->getFullPath($this->_getCropPath($w, $h));
        $image = Image::make($this->getBasePath() . $this->getFullPath() . $this->url)->widen($w);
        $variant = rand(0,2);
        if($variant == 1){
            $watermark = Image::make(App::make('pie_comments.assets')->getPath('img/watermark-1.png'))->widen($w);
        } elseif ($variant == 2){
            $watermark = Image::make(App::make('pie_comments.assets')->getPath('img/watermark-2.png'))->widen($w);
        } else {
            $watermark = Image::make(App::make('pie_comments.assets')->getPath('img/watermark.png'))->widen($w);
        }
        $image->insert($watermark,'top-left',0,0);
        Storage::makeDirectory($cropPath);
        Storage::makeDirectory(dirname($cropPath . $this->url));
        $image->save($this->getBasePath() . $cropPath . $this->url);

        return $this->_getCropPath($w, $h) . $this->url;
    }

    public function getAvatar($w = null, $h = null)
    {
        if(!(isset($w) || isset($h))) {
            return $this->getUrl($this->url);
        }

        if(file_exists($this->getBasePath() . $this->getFullPath($this->_getCropPath($w, $h)) . $this->url)) {
            return $this->getUrl($this->_getCropPath($w, $h) . $this->url);
        }

        return $this->getUrl($this->prepareAvatar($w, $h));
    }
}
