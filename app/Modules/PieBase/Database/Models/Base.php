<?php
namespace App\Modules\PieBase\Database\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 19.07.2016
 * Time: 11:41
 */
class Base extends Model
{
    const STATUS_NOT_CHK      = 0;
    const STATUS_ACTIVE       = 1;
    const STATUS_DELETED      = 3;

    protected $stateClass = [
        0 => 'btn-warning',
        1 => 'btn-success',
        3 => 'btn-danger'
    ];

    protected $stateName = [
        0 => 'InActive',
        1 => 'Active',
        3 => 'Deleted',
    ];

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    public function getStateClass() {
        return 'btn btn-xs ' . $this->stateClass[$this->status];
    }

    public function getStateName() {
        return $this->stateName[$this->status];
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeleted($query)
    {
        return $query->where('status', 3);
    }

    public function scopeNotDisable($query)
    {
        return $query->where('status', '!=', self::STATUS_NOT_CHK);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotDeleted($query)
    {
        return $query
            ->where('status', '!=', self::STATUS_DELETED);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetAll($query)
    {
        return $query->take(5000);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCreatedBy($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @param $value
     * @return string
     */
    protected static function transliterate($value)
    {
        $value = (string)$value;
        $value = strip_tags($value);
        $value = str_replace(array("\n", "\r"), " ", $value);
        $value = preg_replace("/\s+/", ' ', $value);
        $value = trim($value);
        $value = function_exists('mb_strtolower') ? mb_strtolower($value) : strtolower($value);
        $value = strtr($value, [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'j',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ы' => 'y',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'ъ' => '',
            'ь' => ''
        ]);
        $value = preg_replace("/[^0-9a-z-_ ]/i", "", $value);
        $value = str_replace(" ", "-", $value);
        return $value;
    }
}