<?php
namespace App\Modules\PieBase\Http\Services;


class Language extends Base {
    protected $orderBy = [
        'id' => 'desc'
    ];
    protected $modelName = 'App\Modules\PieBase\Database\Models\Language';

    public function getAllShortName(){
        $collection = $this->getAll();
        return $collection->map(function($model){
           return $model->short_name;
        })->toArray();
    }
}