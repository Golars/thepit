<?php
namespace App\Modules\PieBase\Http\Middleware;

use Closure;
use App\Modules\PieBase\Http\Services\User as Service;

class Ware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $service = new Service();
        $user = $service->authByToken($request->session()->get('token'));
        if($user) {
            $request->setUserResolver(function() use ($service){ return $service->getModel();});
            return $next($request);
        }
        return $next($request);
    }
}
