
<!-- sidebar menu -->

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

	<div class="menu_section">
		<ul class="nav side-menu" style="padding-top: 80px;">
			<li>
				<a href="{{route('admin:index')}}"><i class="fa fa-home"></i> {{trans('pie_base::main.menu_main')}}</a>
			</li>
			<li>
				<a>
					<i class="fa fa-newspaper-o"></i> {{trans('pie_article::main.menu_news')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:news:index')}}">{{trans('pie_article::main.sub_menu_news')}}</a>
						<a href="{{route('admin:news:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:categories:index')}}">{{trans('pie_article::main.sub_menu_category')}}</a>
						<a href="{{route('admin:categories:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a>
					<i class="fa fa-pagelines"></i> {{trans('pie_static::main.menu_static')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:static:index')}}">{{trans('pie_static::main.sub_menu_static')}}</a>
						<a href="{{route('admin:static:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a>
					<i class="fa fa-image"></i> {{trans('pie_base::main.menu_media')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:media:index')}}">{{trans('pie_base::main.sub_menu_media')}}</a>
						<a href="{{route('admin:media:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a>
					<i class="fa fa-empire" aria-hidden="true"></i> {{trans('pie_sponsors::main.title')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:sponsors:index')}}">{{trans('pie_sponsors::main.sub_title')}}</a>
						<a href="{{route('admin:sponsors:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
			<li>
				<a>
					<i class="fa fa-cogs"></i> {{trans('pie_base::main.menu_config')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:config:index')}}"><i class="fa fa-cog"></i> {{trans('pie_base::main.sub_menu_config')}}</a></li>
				</ul>
			</li>
			<li>
				<a>
					<i class="fa fa-users"></i> {{trans('pie_base::main.menu_users')}}<span class="fa fa-chevron-down"></span></span>
				</a>
				<ul class="nav child_menu" style="display: none">
					<li><a href="{{route('admin:users:index')}}">{{trans('pie_base::main.sub_menu_users')}}</a>
						<a href="{{route('admin:users:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
					<li><a href="{{route('admin:roles:index')}}">{{trans('pie_base::main.sub_menu_roles')}}</a>
						<a href="{{route('admin:roles:add')}}" class="pull-right create fa fa-plus-circle"></a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<!-- /sidebar menu -->