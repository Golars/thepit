<?php
namespace App\Modules\PieSponsors\Http\Services;

use App\Modules\PieBase\Http\Services\Files;

class Cover extends Files
{
    protected $modelName = 'App\Modules\PieSponsors\Database\Models\Cover';
    protected $orderBy = [];
    protected $hasStatus = false;
}