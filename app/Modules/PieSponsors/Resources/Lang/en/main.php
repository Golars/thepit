<?php
return [
    'title' => 'Sponsors',
    'sub_title' => 'Sponsor\'s List',
    'admin_name' => 'Name',
    'admin_link' => 'Link',
    'admin_emblem' => 'Emblem',
];