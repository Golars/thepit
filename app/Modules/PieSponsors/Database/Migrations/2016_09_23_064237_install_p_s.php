<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstallPS extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sponsors', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('link');
			$table->integer('cover_id')->references('id')->on('files');
			$table->timestamps();
			$table->tinyInteger('status')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sponsors');
	}
}
