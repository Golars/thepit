<?php
namespace App\Modules\PieSponsors\Database\Models;

use App\Modules\PieMedia\Database\Models\File;

class Cover extends File
{
    protected $path = 'sponsors/';
}
