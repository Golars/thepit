@extends('pie_base::admin.layouts.edit')

@section('title_name', trans('pie_base::main.sub_menu_media'))

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_type')) !!}
        {!! Form::select('type',$types,$model->type, ['class'=>'form-control','id' => 'type']) !!}
    </div>
    <div class="form-group" id="video-link" style="display: none">
        {!! Form::label(trans('pie_base::main.link')) !!}
        <div class="input-group">
            {!! Form::text('link', ($model->link)?$model->link->link:'', ['class'=>'form-control']) !!}
            <div class="input-group-btn">
                <button class="btn btn-default" type="button" id="video-check"><i class="fa fa-check-circle-o"
                                                                                  aria-hidden="true"></i> Check
                </button>
            </div>
        </div>
        @if($model->link && $model->link->isYouTubeVideo($model->link->link))
            <div class="form-group iframe">
                <iframe src="{{$model->link->link}}" class="embed-responsive-item"></iframe>
            </div>
        @endif
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_cover')) !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="fa fa-edit"></i> {{trans('pie_base::main.edit_photo')}}
                </div></span>
            {!! Form::file('file',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getFile()}}" width="250" class="img-responsive">
        </div>
    </div>
    <div class="form-group grid">
        {{--{!! Form::file('files[]',['class'=>'','id'=>'selectFiles','multiple'=>true]) !!}--}}
        @foreach($model->files as $file)
            <div id='el{{$file->id}}' class='elementPhoto col-md-2 col-sm-4 col-xs-6 panel grid-item'>
                <img src='{{$file->getCover()}}' class='img-responsive'>
                <span class='deletePhoto'>
                    <div class='middle'>
                        <i class='glyphicon glyphicon-trash'></i> {{trans('pie_base::main.delete_photo')}}
                    </div>
                </span>
                <input class='form-control' name='photo_ids[]' type='hidden' value='{{$file->id}}'>
            </div>
        @endforeach
    </div>
    <div class="panel" id="addPhoto">
        <span class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> {{trans('pie_base::main.add_photo')}}</span>
        <div class="form-group">
            {!! Form::file('file_photo[]', ['class' => 'hidden', 'id' => 'coverInput','multiple' => 'true']) !!}
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{$app['pie_base.assets']->getPath('js/lib/underscore/underscore-min.js')}}"></script>
    @yield('add_script')
    @yield('media_script')
    <script>
        function loadType(self) {
            if (self.val() == 1) {
                $('.grid').fadeOut().fadeIn('slow');
                $('#video-link').fadeOut('fast');
                $('#addPhoto').fadeOut().fadeIn('slow');
            } else {
                if (true) {
                }
                $('.grid').fadeOut('fast');
                $('#addPhoto').fadeOut('fast');
                $('#video-link').fadeOut('fast').fadeIn('slow');
            }
        }

        $(document).on('ready', function () {
            loadType($('#type'));
        });

        var iframe = function (link) {
            return '<div class="form-group iframe">' +
                    '<iframe src="' + link + '" class="embed-responsive-item"></iframe>' +
                    '</div>';
        }

        $('#video-check').on('click', function () {
            $.ajax({
                url: "/admin/media/api/link",
                type: 'post',
                dataType: 'json',
                data: {
                    'text': $('#video-link').find('input').val()
                },
                success: function (response) {
                    if (response.youtube) {
                        $('#video-link').find('input').val(response.text);
                        $('#video-link').find('.iframe').remove();
                        $('#video-link').append(iframe(response.text));
                    }
                },
                error: function () {
                    console.log('upload fail');
                }
            });
        });

        $('#type').on('change', function () {
            var self = $(this);
            loadType(self);
        });
        $('#addPhoto span').on('click', function () {
            $('#coverInput').click();
            return;
        });
        $('#coverInput[type=file]').on('change', function () {
            PopUpShow();
            var formData = new FormData();
            if (this.files[0] == undefined) {
                return new PNotify({
                    title: 'Ошибка',
                    text: 'Файл не найден',
                    type: 'error'
                })
            }
            $.each(this.files, function (i, v) {
                formData.append('file_photo[]', v);
            });
            $.ajax({
                type: 'POST',
                url: '/admin/media/add_photo',
                data: formData,
                processData: false,
                contentType: false,
                success: function (res) {
                    PopUpHide();
                    var response = JSON.parse(res);
                    $.each(response.collection, function (i, item) {
                        $item = photo_element({image: item.image, id: item.id});
                        $('.grid').append($item);
                        $('#el' + item.id).find('.deletePhoto').on('click', function () {
                            deletePhoto(this);
                        });
                    });

                    new PNotify({
                        title: 'Успех',
                        text: 'Добавление данных прошло успешно!',
                        type: 'success'
                    });
                },
                error: function (res) {
                    PopUpHide();
                    new PNotify({
                        title: 'Ошибка',
                        text: 'Фото не загружено',
                        type: 'error'
                    });
                    return false;
                }
            });
        });

        var deletePhoto = function (self) {
            var rootEl = $(self).parent();
            var id = rootEl.find('input').val();

            $.ajax({
                type: 'DELETE',
                url: '/admin/media/del_photo/' + id,
                success: function (res) {
                    new PNotify({
                        title: 'Успех',
                        text: 'Удаление данных прошло успешно!',
                        type: 'success'
                    });
                    rootEl.slideToggle(500);
                    setTimeout(function () {
                        rootEl.remove();
                    }, 500);
                },
                error: function (res) {
                    var errors = JSON.parse(res.responseText).errors || ['Данные не валидные'];
                    new PNotify({
                        title: 'Ошибка',
                        text: errors.join("<br>"),
                        type: 'error'
                    });
                    return false;
                }
            });
        };

        $('.elementPhoto .deletePhoto').on('click', function (e) {
            deletePhoto(this);
        });
        var photo_element = _.template("<div id='el<%=id %>' class='elementPhoto col-md-2 col-sm-4 col-xs-6 panel grid-item'>"+
                    "<img src = '<%= image %>' class='img-responsive'>"+
                    "<span class='deletePhoto'><div class='middle'><i class='glyphicon glyphicon-trash'></i> Удалить фото</div></span>"+
                    "<input class='form-control' name='photo_ids[]' type='hidden' value='<%=id %>'></div>");
    </script>
@endsection