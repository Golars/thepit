@extends('the_pit::layouts/page')
@section('page')
    <embed src="{{$link}}" style="width: 100%; min-height:800px" />
    <hr>
    <div class="text-center">
        <a href="{{$link}}" target="_blank">Download</a>
    </div>
@endsection