<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMovies extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links_for_media', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('media_id')->references('id')->on('media');
			$table->tinyInteger('status')->default(1);
			$table->string('link');
			$table->timestamps();
		});
		Schema::table('media', function (Blueprint $table) {
			$table->tinyInteger('type')->default(1); // in future fixes
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('links_for_media');
	}
}
