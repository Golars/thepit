<?php
namespace App\Modules\PieMedia\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use App\Modules\PieMedia\Http\Services\File;
use App\Modules\PieMedia\Http\Services\FilesToMedia;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 10:46
 */
class MediaController extends Controller
{
    protected $prefix = 'admin:media';
    protected $moduleName = 'pie_media';
    protected $serviceName = 'App\Modules\PieMedia\Http\Services\Media';

    public function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'types' => App::make('App\Modules\PieMedia\Http\Services\Media')->prepareSelectTypes(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'type' => 'required',
            'file_id' => '',
        ]);
        if ($request->method() != 'GET' && isset($request['file'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_id' => $imageId));
        }
        $result = parent::add($request);
        if (isset($this->service->getModel()->id)) {
            $this->saveMedia($request, $this->service->getModel()->id);
        }

        if ($request->input('link')) {
            $this->saveLink($request, $this->service->getModel()->id);
        }
        return $result;
    }

    protected function saveLink(Request $request, $id)
    {
        $relate = new App\Modules\PieMedia\Database\Models\LinksToMedia();
        $model = $relate->query()->where('media_id', $id)->first();
        if ($model) {
            $model->update(['link' => $request->input('link')]);
        } else {
            $relate->create(['media_id' => $id, 'link' => $request->input('link')]);
        }
    }
    public function edit(Request $request, $id)
    {
        $this->setRules([
            'name' => 'required|min:2',
            'type' => 'required',
            'file_id' => '',
        ]);

        $this->saveMedia($request, $id);
        if ($request->hasFile('file')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_id' => $imageId));
        }
        if ($request->input('link')) {
            $this->saveLink($request, $id);
        }
        return parent::edit($request, $id);
    }

    private function saveFile(Request $request)
    {
        $service = new File($request->file('file'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }

    /**
     * Save Files Into Relation Table
     * @param Request $request
     * @param $id
     */
    private function saveMedia(Request $request, $id)
    {
        if (isset($request['photo_ids'])) {
            foreach ($request['photo_ids'] as $key => $file_id) {
                $relation = new FilesToMedia();
                $model = $relation->getModel();
                $relation->setWhere(['file_id' => $file_id]);
                if (!isset($relation->getOne()->id)) {
                    $model->create([
                        'file_id' => $file_id,
                        'media_id' => $id
                    ]);
                }
            }
        }
    }

    public function addPhoto(Request $request)
    {
        if (!$request->hasFile('file_photo')) {
            return $this->sendWithErrors('Image is not found', 404);
        }
        $images = [];
        foreach ($request->file('file_photo') as $file) {
            $images[] = $this->savePhoto($request, $file);
        }
        $images = array_map(function ($model) {
            return [
                'image' => $model->getCover(),
                'id' => $model->id];
        }, $images);

        return $this->sendOk(['collection' => $images]);
    }

    private function savePhoto(Request $request, $file)
    {
        $service = new File($file);
        $service->saveFile($request->user()->id);
        return $service->getModel();
    }

    /**
     * Delete File from AJAX
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function deletePhoto(Request $request, $id)
    {
        if (!$id) {
            return $this->sendWithErrors('Id is incorect', 404);
        }
        $service = new File();
        $model = $service->getOne($id);
        if (!$model->id) {
            return $this->sendWithErrors('Not found file', 404);
        }
        $relation = new FilesToMedia();
        $relation->setWhere(['file_id' => $model->id]);
        $relation->getOne()->delete();
        $model->delete();
    }

    /**
     * @param Request $request
     * @return App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function uploadLink(Request $request)
    {
        $links = new App\Modules\PieMedia\Database\Models\LinksToMedia();
        if($request->input('text')){
            return $this->sendOk([
                'text' => $links->saveYouTubeLink($request->input('text')),
                'youtube' => $links->isYouTubeVideo($request->input('text'))
            ]);
        }
        return $this->sendWithErrors('fail');
    }
}