<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 25.07.2016
 * Time: 11:22
 */

namespace App\Modules\PieMedia\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class FilesToMedia extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieMedia\Database\Models\FilesToMedia';
    protected $hasStatus = true;

    /**
     * @param $media_id
     * @param $file_id
     * @param int $user_id
     * @return static
     */
    public function saveRelation($media_id, $file_id, $user_id = 0)
    {
        return $this->getModel()->create([
            'media_id' => $media_id,
            'file_id' => $file_id,
            'user_id' => $user_id
        ]);
    }
    
    
}
