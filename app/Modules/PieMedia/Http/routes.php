<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function() {
	Route::group(['middleware' => 'JournalistAuth'], function () {
		Route::group(['prefix' => 'media'], function () {
			$asAction = ':media';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\MediaController@index']);
			Route::any('/add', ['as' => $asAction.':add', 'uses' => 'Admin\MediaController@add']);
			Route::get('/{id}/active', ['as' => $asAction.':active', 'uses' => 'Admin\MediaController@active']);
			Route::any('/{id}/edit', ['as' => $asAction.':edit', 'uses' => 'Admin\MediaController@edit']);
			Route::post('/add_photo',['as' => $asAction . ':add:photo', 'uses' => 'Admin\MediaController@addPhoto']);
			Route::delete('/del_photo/{id}',['as' => $asAction . ':dalete:photo', 'uses' => 'Admin\MediaController@deletePhoto']);
			Route::post('/api/link',['as' => $asAction . ':api:link', 'uses' => 'Admin\MediaController@uploadLink']);
		});
	});
});
Route::group(['as' => 'site:', 'middleware' => ['webAdmin']], function() {
	Route::get('/text/{id}', ['as' => 'text', 'uses' => 'MediaController@loadContent']);
	Route::get('/photo/{id}', ['as' => 'photo', 'uses' => 'MediaController@loadPhoto']);
});

