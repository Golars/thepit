<?php
namespace App\Modules\BlueBerryPie\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Http\Request;

class PieAnswer extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\BlueBerryPie\Database\Models\PieAnswer';

}