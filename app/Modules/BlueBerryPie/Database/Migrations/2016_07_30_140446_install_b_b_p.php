<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstallBBP extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pie_games', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('user_id');
			$table->integer('file_id')->default(0);
			$table->integer('article_id')->default(0);
            $table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
		Schema::create('pie_quest', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('game_id');
			$table->boolean('is_first')->default(false);
			$table->text('question');
            $table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
		Schema::create('pie_answer', function (Blueprint $table) {
			$table->increments('id');
			$table->text('answer');
			$table->integer('quest_id');
			$table->integer('answer_id');
            $table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pie_games');
		Schema::dropIfExists('pie_quest');
		Schema::dropIfExists('pie_answer');
	}
}
