<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 30.07.2016
 * Time: 18:19
 */

namespace App\Modules\BlueBerryPie\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class PieAnswer extends Base
{
    protected $perPage = 15;

    public $timestamps = true;
    protected $table = 'pie_answer';

    protected $fillable = array(
        'answer',
        'quest_id',
        'answer_id',
        'status',
        'value'
    );

    public function question_relation()
    {
        return $this->belongsTo('\App\Modules\BlueBerryPie\Database\Models\PieQuest', 'quest_id');
    }

    public function answer_relation()
    {
        return $this->belongsTo('\App\Modules\BlueBerryPie\Database\Models\PieQuest', 'answer_id');
    }

    public function getInfo()
    {
        return [
            'id' => $this->id,
            'variant' => $this->answer,
            'value' => ($this->value) ?: 0,
            'answer' => ($this->answer_relation) ? $this->answer_relation->id : ''
        ];
    }
}