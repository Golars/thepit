<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 30.07.2016
 * Time: 18:19
 */

namespace App\Modules\BlueBerryPie\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class PieGame extends Base
{
    protected $with = [
        'user',
        'article',
        'cover'
    ];

    protected $perPage = 15;

    public $timestamps = true;
    protected $table = 'pie_games';

    protected $fillable = array(
        'user_id',
        'name',
        'file_id',
        'article_id',
        'status',
    );

    public function article(){
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Articles','article_id');
    }

    public function user(){
        return $this->belongsTo('\App\Modules\PieBase\Database\Models\User','user_id');
    }

    public function cover(){
        return $this->belongsTo('\App\Modules\BlueBerryPie\Database\Models\PieCover','file_id');
    }

    public function getCover($w = null){
        return ($this->cover) ? $this->cover->getCover($w) : App('logo');
    }

    public function quests()
    {
        return $this->hasMany('\App\Modules\BlueBerryPie\Database\Models\PieQuest',
            'game_id','id');
    }

    public function answers()
    {
        return $this->hasOne('\App\Modules\BlueBerryPie\Database\Models\PieQuest',
            'game_id','id')->where('is_first',true);
    }

    public function getInfo()
    {
        return [
            'name'=> $this->name,
            'graph'=>$this->answers->getInfo()
        ];
    }
    
}