@extends('the_pit::layouts/blank')
@section('title')
    Games | {{$game['name']}}
@endsection
@section('keywords','game')
@section('description','game')
@section('styles')
    <style>
        .block {
            width: 100%;
            height: 100%;
            display: table;
        }

        .question {
            padding: 25px;
            text-align: justify;
            vertical-align: middle;
            min-height: 100px;
        }

        .check {
            min-height: 100px;
            display: none;
            padding: 25px;
            font-size: 2em;
            vertical-align: middle;
            color: #2ca02c;
        }

        .info {
            cursor: crosshair;
            padding: 15px;
            transition: .2s;
        }

        .info:hover {
            box-shadow: 0 0 10px #000;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h1>Game</h1>
        <div class="block col-xs-12">
            <div class="question col-xs-12">
                {{$game['graph']['question']}}
            </div>
            <div class="answers col-xs-12">
                @foreach($game['graph']['answer'] as $answer)
                    <div class="col-xs-6 panel info" data-text="{{$answer['answer']}}">
                        <span class="text-mutted">
                            {{$answer['variant']}}
                        </span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var block = $('.block');
        var variant = function (data, value) {
            return '<div class="col-xs-6 panel info" data-text="' + data + '">' +
                    '<span class="text-mutted">' + value +
                    '</span>' +
                    '</div>';
        }
        qW = function (self) {
            $.each($('.info'), function (k, v) {
                $(v).hide();
            });
            $('.answers').html('<span class="check"><i class="fa fa-check-circle"></i>Accepted</span>');
            var answerId = $(self).attr('data-text');
            $('.check').fadeIn('slow', function () {
                $('.check').fadeOut('slow', function () {
                    qGo(answerId);

                })
            });
        }
        function qGo($id) {
            $.ajax({
                url: "/berry/walk/" + $id,
                dataType: 'json',
                success: function (response) {
                    if (response.gq) {
                        block.find('.question').empty();
                        block.find('.answers').empty();
                        block.find('.question').append('' +
                                '<span class="">' +
                                response.gq.question +
                                '</span>');
                        if (response.gq.answer[0]) {
                            $.each(response.gq.answer, function (k, v) {
                                block.find('.answers').append(variant(v.answer, v.variant));
                                $('.info').on('click', function () {
                                    qW(this);
                                })
                            })
                        }
                    }
                }
            })
        }
        $(document).ready(function () {
            $('.info').on('click', function () {
                qW(this);
            });
        });
    </script>
@endsection