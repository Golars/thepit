@extends('the_pit::layouts/blank')
@section('title')
    Games | Edit
@endsection
@section('keywords','game')
@section('description','game')
@section('styles')
    <style>
        .block {
            width: 100%;
            height: 100%;
            display: table;
        }

        .question {
            padding: 25px;
            text-align: justify;
            vertical-align: middle;
            min-height: 100px;
        }

        .check {
            min-height: 100px;
            display: none;
            padding: 25px;
            font-size: 2em;
            vertical-align: middle;
            color: #2ca02c;
        }
    </style>
@endsection
@section('content')
    {!! Form::open(['class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) !!}
    @foreach($collection as $model)
        <div class="container">
            <div class="form-group has-success alert alert-success">
                {!! Form::textarea('question[]',$model->question,['class'=>'form-control bg-info','required']) !!}
                {!! Form::number('rule[]',$model->rule) !!}
                {!! Form::hidden('q_id[]',$model->id) !!}
            </div>
            <div class="answer">
                @foreach($model->answer as $answer)
                    <div class="form-group has-info col-xs-4">
                        {!! Form::hidden('a_id[]',$answer->id) !!}
                        {!! Form::textarea('answer[]',$answer->answer,['class'=>'form-control','required']) !!}
                        {!! Form::number('value[]',$answer->value,['class'=>'form-control','required']) !!}
                        {!! Form::hidden('answer_id[]',$answer->answer_id) !!}
                        {!! Form::textarea('next_question[]',($answer->answer_relation)?$answer->answer_relation->question:'',['class'=>'form-control','required']) !!}
                    </div>
                @endforeach
            </div>
            <span class="btn btn-success add"><i class="fa fa-plus"></i> Add</span>
        </div>
    @endforeach
    {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop
@section('scripts')
    <script src="{{$app['pie_base.assets']->getPath('js/lib/underscore/underscore-min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.add').on('click', function () {
                $(this).parent().find('.answer').append($block);
            });
        });
        var $block = _.template('<div class="form-group has-info col-xs-4">' +
                '<input name="a_id[]" type="hidden" value="">' +
                '<textarea class="form-control" required="required" name="answer[]" cols="50" rows="10"></textarea>' +
                '<input class="form-control" required="required" name="value[]" type="number" value="">' +
                '<input type="hidden" name="answer_id[]">' +
                '<textarea class="form-control" required="required" name="next_question[]" cols="50" rows="10"></textarea>' +
                '</div>')
    </script>
@stop