<?php
namespace App\Modules\ThePit\Providers;

use App\Modules\PieBase\Providers\Module;
use Caffeinated\Modules\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{

	protected $moduleName = 'the_pit';
	protected $assetsPath = 'assets/the_pit/';
	/**
	 * This namespace is applied to the controller routes in your module's routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Modules\ThePit\Http\Controllers';

	/**
	 * Define your module's route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router $router
	 * @return void
	 */
	public function boot(Router $router)
	{

		$this->initAssets();
		parent::boot($router);

		//
	}
	protected function initAssets(){
		$this->publishes([
			__DIR__.'/../Resources/Assets' => public_path($this->assetsPath),
		], $this->moduleName);

		$this->app->bind('the_pit.assets', function() {
			return new Module($this->assetsPath);
		});
		$this->app->bind('pit', function() {
			return (new Module($this->assetsPath))->getPath('images/logo.png');
		});
		$this->app->bind('big_pit', function() {
			return (new Module($this->assetsPath))->getPath('images/biglogo.png');
		});
		$this->app->bind('full_fill', function() {
			return (new Module($this->assetsPath))->getPath('images/fill.png');
		});
	}

	/**
	 * Define the routes for the module.
	 *
	 * @param  \Illuminate\Routing\Router $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => $this->namespace], function($router)
		{
			require (config('modules.path').'/ThePit/Http/routes.php');
		});
	}
}
