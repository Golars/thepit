@extends('the_pit::layouts/page')
@section('css')
    <style>
        .f-video-editor {
            display: block;
            width: 100%;
            text-align: center;
        }
    </style>
@endsection
@section('page')
    <?php $translate = $content->getTranslatePage($lang); ?>
    <div class="container">
        <div class="row">
            <h1 class="page_title_main">{{$translate->title}}</h1>
            @if($content->display_image)
                <img src="{{$translate->getCover(450)}}" class="img-respomsive">
            @endif
            <div>{!! $translate->content !!}</div>
        </div>
    </div>
@endsection