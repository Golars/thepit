@extends('the_pit::layouts/blank')
@section('title','The PIT | Main page')
@section('keywords','postapocaliptic')
@section('description','postapocaliptic')
@section('styles')
    <style>
        .navbar-transparent {
            background-color: transparent;
        }

        .fa-reorder {
            font-size: 2em;
            color:#DC7A23;
        }

        .page-scroll {
            color: #DC7A23;
            font-size: 1.5em;
            font-weight: bold;
            transition: .5s;
            background: rgba(0, 0, 0, .7);
            text-align: center;
        }

        .page-scroll:hover {
            color: #DC7A23;
            text-shadow: 2px 2px #2B2A28;
        }

        .try{
            font-size: .5em;
        }
        .navbar-fixed-top{
            padding:0;
        }
        @media (min-width: 768px) {
            .try{
                font-size:1em;
            }
            .navbar-fixed-top{
                padding: 50px;
            }
            .page-scroll {
                background-color: transparent !important;
            }

            .page-scroll:hover {
                transform: scale(1.2);
            }
        }

        html, body {
            height: 100%;
        }

        body {
            background-color: #2B2A28;
            font-weight: 100;
            margin: 0;
            padding: 0;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            overflow: auto;
        }
    </style>
@endsection
@section('content')
    <video autoplay loop muted class="bgvideo" id="bgvideo" poster="/images/bg.jpg">
        <source src="/video/video.mp4" type="video/mp4">
    </video>
    <nav class="navbar navbar-transparent navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <i class="fa fa-reorder"></i>
                </button>
                <span class="navbar-brand" href="#"></span>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @include('the_pit::layouts.block.menu_list')
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <a href="http://pie.sticky.loc" class="page-scroll lang">en</a>
                    <a href="http://ru.pie.sticky.loc" class="page-scroll lang">ru</a>
                    {{--<a href="http://ua.pie.sticky.loc">ua</a>--}}
                </ul>
            </div>
        </div>
    </nav>
    <div class="container container-a1">
        <div class="content">
            <div class="title col-xs-10 col-xs-offset-2">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <img src="{{$app['big_pit']}}" width="190" class="img-responsive">
                </div>
                <div class="col-xs-12" style="font-size: 18px">
                    {{(Request::user())?Request::user()->getFullName():''}}
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12 try">
                    TRY TO SURVIVE
                </div>
                <div class="col-xs-12">
                    <span class="btn btn-fucker" id="buy">{{trans('the_pit::main.body_buy')}}</span>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-transparent navbar-fixed-bottom">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right" style="padding-right: 100px">
                    <li>
                        <a href="" class="social-btn"><i class="fa fa-vk"></i></a>
                    </li>
                    <li>
                        <a href="" class="social-btn"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="popup">
        <div class="container container-a2">
            <div style="width: 100%;
    text-align: center;">
                <div class="title">
                    <h1>Coming soon</h1>
                    <span class="btn btn-fucker" id="close_popup"><i class="fa fa-times"></i> Close</span>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $('#buy').on('click', function () {
            $('.popup').fadeIn('slow');
        })
        $("#close_popup").on('click', function () {
            $('.popup').fadeOut('slow');
        })
    </script>
@endsection



