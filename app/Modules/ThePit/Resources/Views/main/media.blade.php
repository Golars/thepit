@extends('the_pit::layouts/page')
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
@endsection
@section('page')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title_main">{{trans('pie_base::main.menu_media')}}</h1>
                @if(isset($collection))
                    <div class="grid" id="media">
                        @foreach($collection as $media)
                            @include('the_pit::layouts/block/media')
                        @endforeach
                    </div>
                    {!! $collection->render() !!}
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/masonry.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/imagesloaded.pkgd.min.js')}}"></script>
    <script>
        function croper() {
            $.each($(".grid-item"), function (k, v) {
                $(v).width($(v).width() - 20);
            });
        }
        isotope = function () {
            croper();
            var $grid = $("#media").masonry({
                percentPosition: true,
                transitionDuration: '0.2s',
                gutter: 5
            });
            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });
        };
        $(document).ready(function () {
            isotope();
        });
    </script>
@endsection