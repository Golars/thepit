@extends('the_pit::layouts/page')
@section('css')
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="{{$app['pie_base.assets']->getPath('/css/loader.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
@endsection
@section('title')
    {{$model->name}} | The PIT
@endsection
@section('page')
    <?php $translate = $model->getTranslateArticle($lang); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page_title_main">{{$translate['name']}}</h1>
            </div>
            <div class="col-lg-offset-1 col-lg-10">
                <div class="page-header">
                    <h1 class="">{{$translate['info']}}</h1>
                    <span class="article_date" data-text="{{$model->created_at}}" style="float: right;margin-top: -10px;">
                        {{$model->created_at}}
                    </span>
                </div>
                <div style="margin: 0 auto; display: block;" class="col-xs-12">
        <span style="margin: 0 auto; display: table;">
            <img src="{{$model->getCover(650)}}" class="img-responsive" style="max-height: 450px;">
            <div class="">
                <a href="{{route('site:news',['category'=>$model->category->id])}}" class="tag text-left"
                   style="background-color: {{$model->category->color}};">
                    {{$model->category->name}}
                </a>
            </div>
        </span>
                </div>
                <div class="col-xs-12">
                    {!! $translate['text'] !!}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <section id="comments_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-2 col-lg-8">
                    <h2 class="bronx"><i class="fa fa-comments"></i> {{trans('the_pit::main.page_comments')}}</h2>
                    <section id="comment_block">
                    </section>
                    <div class="col-xs-12">
                        @if(Request::user())
                            <form class="form-inline" action="{{route('site:api:add:comment',$model->id)}}"
                                  method="POST"
                                  id="comment">
                                <div class="col-xs-1 comment-feedback-image">
                                    <img src="{!! Request::user()->getCover(50) !!}" class="img-circle">
                                </div>
                                <div class="col-xs-11 comment-feedback-text">
                                    <textarea class="comment-text" spellcheck="true" name="comment_text"
                                              required></textarea>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_user_id" value="{{ Request::user()->id }}">
                                <button type="submit" class="btn btn-fucker btn-fucker-a" id="comment_btn"
                                        style="float: right;">{{trans('the_pit::main.page_leave_comment')}}</button>
                                <a href="{{route('site:socialite:logout')}}" class="btn btn-fucker btn-fucker-a">logout</a>
                            </form>
                        @else
                            <div class="text-center">
                    <span class="btn btn-fucker btn-fucker-a"
                          id="auth">{{trans('the_pit::main.page_leave_comment')}}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="bronx">{{trans('the_pit::main.page_see_more')}}</h2>
                <div id="grid">
                    @if(isset($actual) && $actual)
                        @foreach($actual as $article)
                            @include('the_pit::layouts/block/regular')
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="b-popup" id="loading" style="z-index: 999">
        <div class="b-popup-content">
            <div id="container">
                <div class="big-circle circle">
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                    <div class="inr-circle circle"></div>
                </div>
                <div class="top-bar bar"></div>
                <div class="mid-bar bar"></div>
                <div class="btm-bar bar"></div>
            </div>
        </div>
    </div>
    <section class="popup" id="auth_form">
        <div class="container container-a2">
            <div style="width: 100%; text-align: center;">
                <div class="title" style="font-size: 1.5em">
                    <h2>{{trans('the_pit::main.page_leave_comment')}}</h2>
                    <div class="col-xs-12">
                        <a href="{{route('site:socialite.auth','vk')}}" class="social-btn social-auth"
                           style="text-decoration: none">
                                        <span class="btn btn-fucker btn-fucker-a" style="letter-spacing: 5px;">
                                            <i class="fa fa-vk"></i> VK
                                        </span>
                        </a>
                        <a href="{{route('site:socialite.auth','facebook')}}" class="social-btn social-auth"
                           style="text-decoration: none">
                                        <span class="btn btn-fucker btn-fucker-a" style="letter-spacing: 5px;">
                                    <i class="fa fa-facebook"></i> Facebook
                                        </span>
                        </a>
                    </div>
                    <form class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1"
                          method="post" action="{{route('site:socialite:login')}}" id="auth_form_dom">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group-alt">
                            <input type="text" name="auth_name" id="auth_name"
                                   required placeholder="{{trans('the_pit::main.form_login_email')}}">

                        </div>
                        <div class="form-group-alt">
                            <input type="password" name="auth_pass" id="auth_pass"
                                   placeholder="{{trans('the_pit::main.form_password')}}">
                        </div>
                        <div style="margin-top: 1em ">
                            <button type="submit" class="btn btn-fucker btn-fucker-a" id="auth_login">
                                {{trans('the_pit::main.page_login')}}
                            </button>
                        </div>
                    </form>
                                <span id="auth_form_close"
                                      class="btn btn-fucker">{{trans('the_pit::main.page_close')}}</span>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/masonry.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/in_page_js.js')}}"></script>
    <script>
        function momentoForDates() {
            $.each($(".article_date"), function (i, v) {
                var time = moment($(v).attr('data-text')).locale('<?=(App::getLocale() != 'pi') ? App::getLocale() : 'en'?>');
                $(v).text(time.fromNow());
            });
        }
        function croper() {
            $.each($(".grid-item"), function (k, v) {
                $(v).width($(v).width() - 20);
            });
        }
        isotope = function () {
            croper();
            momentoForDates();
            var $grid = $("#grid").masonry({
                percentPosition: true,
                transitionDuration: '0.2s',
                gutter: 5
            });
            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });
        };

        function loadComments(page) {
            PopUpShow();
            $("#more").parent().remove();
            var offset = $('.panel-comment').length
            $.ajax({
                url: "{{route('site:api:load:comments',$model->id)}}?page=" + page + "&offset=" + offset,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    if (response.collection) {
                        $.each(response.collection, function (k, model) {
                            $("#comment_block").append(comment_block(model));
                            clickComment($('.panel-comment').last());
                        });
                        momentoForDates();
                    }
                    if (response.page * response.limit < response.count) {
                        var how = response.count - response.page * response.limit;
                        $("#comment_block").append('<div class="appender">' +
                                ' <span class="btn btn-fucker btn-fucker-a" id="more">[' + how + '] More</span>' +
                                '</div>');
                        clickMore($('#more'));
                    }
                    PopUpHide();
                },
                error: function () {
                    PopUpHide();
                    setTimeout(function () {
                        loadComments(page)
                    }, 1000);
                }
            })
        }

        $(document).ready(function () {
            isotope();
            $("#auth").on('click', function () {
                $("#auth_form").fadeIn('slow');
            });
            $("#auth_form_close").on('click', function () {
                $("#auth_form").fadeOut('slow');
            })
            $("#auth_form_dom").on('submit', function () {
                var $form = $(this);
                var $title = $("#auth_form").find('.title');
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: $form.serializeArray(),
                    success: function (response) {
                        if (response.goto != undefined) {
                            $title.empty()
                            $title.append("<h1>Success</h1><small>Wait few seconds</small>")
                            setTimeout(function () {
                                window.location.href = response.goto;
                            }, 1000);
                        }
                        console.log(response);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                return false;
            });
        })
    </script>
@stop