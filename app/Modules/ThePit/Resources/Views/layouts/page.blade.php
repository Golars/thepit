@extends('the_pit::layouts/blank')
@section('title','The PIT | Main page')
@section('keywords','postapocaliptic')
@section('description','postapocaliptic')
@section('styles')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/page.css')}}">
    @yield('css')
    <style>
        @-webkit-keyframes wow {
            from {
                transform: scale(1);
            }
            to {
                transform: scale(1.5);
            }
        }
        .upper {
            position: fixed;
            bottom: 10px;
            left: 10px;
            font-size: 2em
        }
        .upper a{
            text-decoration: none;
            color: #DC7A23;
        }
        .upper a i{
            transition: .5s;
        }
        .upper a:hover i{
            animation: wow 1s infinite alternate;
            -webkit-animation: wow 1s infinite alternate;
        }
    </style>
@endsection
@section('content')
    <div class="upper">
        <a href="#">
            <i class="fa fa-arrow-circle-o-up"></i>
        </a>
    </div>
    <header>
        <nav class="navbar navbar-transparent" style="z-index: 1001;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a class="navbar-brand" href="{{route('site:index')}}"><img src="{{$app['pit']}}" height="85"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @include('the_pit::layouts.block.menu_list')
                        <li>
                            <span class="btn btn-fucker page-scroll" id="buy">{{trans('the_pit::main.body_buy')}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="header"></div>
    <section id="main">
        @yield('page')
    </section>
    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-2 col-sm-4 col-sm-offset-1 col-xs-offset-3 col-xs-6 text-center"
                     style="z-index:10">
                    <img src="{{$app['big_pit']}}" class="img-responsive">
                    <span class="pit_text">TRY TO SURVIVE</span>
                <span class="lang-block-page">
                    <a href="http://pie.sticky.loc{{\Illuminate\Http\Request::capture()->server('REDIRECT_URL')}}"
                       class="page-scroll lang page-lang">en</a>
                    <a href="http://ru.pie.sticky.loc{{\Illuminate\Http\Request::capture()->server('REDIRECT_URL')}}"
                       class="page-scroll lang page-lang">ru</a>
                    {{--<a href="http://ua.pie.sticky.loc">ua</a>--}}
                </span>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-offset-0 col-sm-offset-1 col-xs-12 text-center"
                     style="z-index:10">
                    <h2 class="pit_text sponsors_text col-xs-12 bronx">{{trans('the_pit::main.page_sponsors')}}</h2>
                    @if(isset($sponsors) && $sponsors && $sponsors->first())
                        @foreach($sponsors as $sponsor)
                            <a class="center" href="{{$sponsor->link}}" target="_blank">
                                <img src="{{$sponsor->getCover()}}"
                                     class="img-responsive sponsors_block col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            </a>
                        @endforeach
                    @else
                        <h2 class="col-xs-12">
                            No sponsors jet
                        </h2>
                    @endif
                </div>
                @include('the_pit::layouts.block.subscribe')
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="text-center">
                Copyright &copy; Selmarinel. All right reserved
            </div>
        </div>
    </footer>
    <section class="popup" id="buy_popup">
        <div class="container container-a2">
            <div style="width: 100%;
    text-align: center;">
                <div class="title">
                    <h1>Coming soon</h1>
                    <span class="btn btn-fucker" id="close_popup"><i
                                class="fa fa-times"></i> {{trans('the_pit::main.page_close')}}</span>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{$app['the_pit.assets']->getPath('/js/moment.min.js')}}"></script>
    @yield('script')
    <script>
        function momentoForDates() {
            $.each($(".article_date"), function (i, v) {
                var time = moment($(v).attr('data-text')).locale('<?=(App::getLocale() != 'pi') ? App::getLocale() : 'en'?>');
                $(v).text(time.fromNow());
            });
        }
        momentoForDates();
        $('#buy').on('click', function () {
            $('#buy_popup').fadeIn('slow');
        });
        $("#close_popup").on('click', function () {
            $('#buy_popup').fadeOut('slow');
        });
    </script>
@endsection
