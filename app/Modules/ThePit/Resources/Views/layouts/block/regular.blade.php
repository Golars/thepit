@if(isset($article))
    <div class="grid-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{route('site:news:article',['id'=>$article->id])}}">
            <img src="{{$article->getCover(350)}}" class="img-responsive">
            @if($article->category)
                <a href="{{route('site:news',['category'=>$article->category->id])}}" class="tag"
                   style="background-color: {{$article->category->color}};">
                    {{$article->category->name}}
                </a>
            @endif
            <div class="article-information">
                <span class="article_date"
                      data-text="{{$article->created_at->setTimeZone('Europe/Kiev')}}">{{$article->created_at}}</span>
                <span class="author">{{$article->getUser()->getFullName()}}</span>
                <?php $translate = $article->getTranslateArticle($lang); ?>
                <span class="article-title">{{$translate['name']}}</span>
                <span class="article-info">{{$translate['info']}}</span>
                <span class="views">
                    <i class="fa fa-eye"></i> {{$article->views}}
                </span>
                <span class="comments">
                    <i class="fa fa-comments-o"></i> {{$article->comments->count()}}
                </span>
            </div>
        </a>
    </div>
@endif