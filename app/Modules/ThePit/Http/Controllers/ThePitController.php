<?php
namespace App\Modules\ThePit\Http\Controllers;

use App;
use App\Modules\PieArticle\Http\Services\Article;
use App\Modules\PieArticle\Http\Services\Category;
use App\Modules\PieBase\Http\Controllers\Controller;
use App\Modules\PieMedia\Http\Services\Media;
use App\Modules\PieTranslator\Http\Services\Languages;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Modules\PieBase\Http\Services\User as User;
use App\Modules\PieStatic\Http\Services\StaticPie;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 20.07.2016
 * Time: 13:00
 */
class ThePitController extends Controller
{
    protected $prefix = 'main';
    protected $moduleName = 'the_pit';

    protected function getMenu()
    {
        $static = new StaticPie();
        return $static->getMenu();
    }

    protected function getSponsors()
    {
        $sponsors = new App\Modules\PieSponsors\Http\Services\Sponsor();
        $sponsors->setWhere(['status' => App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE]);
        return $sponsors->getAll();

    }

    public function indexAction(Request $request)
    {
        return view($this->getViewRoute('index'), [
            'menu' => $this->getMenu(),
        ]);
    }

    public function infoAction(Request $request)
    {
        return view($this->getViewRoute('info'), ['menu' => $this->getMenu()]);
    }

    public function contactsAction(Request $request)
    {
        return view($this->getViewRoute('contacts'), ['menu' => $this->getMenu()]);
    }

    public function newsAction(Request $request, $category = null)
    {
        $articles = new Article();
        $categories = new Category();
        $language = new Languages();
        $language = $language->getModel()->where('name', App::getLocale())->first();
        return view($this->getViewRoute('news'),
            [
                'collection' => $articles->paginateArticles($request, $category, $this->config->findBySlug('paginate_count')),
                'categories' => $categories->prepareSelect(),
                'popular' => $articles->getPopular(),
                'lang' => ($language) ? $language->id : 1,
                'menu' => $this->getMenu(),
                'sponsors' => $this->getSponsors()
            ]);
    }

    public function oneArticleAction(Request $request, $id)
    {
        $article = new Article();
        $language = new Languages();
        $lang = $language->getModel()->where('name', App::getLocale())->first();
        $model = $article->oneArticle($id);
        $language = $model->getTranslateArticle(($lang) ? $lang->id : 1);

        $model->name = $language['name'];
        $model->info = $language['info'];
        $model->text = $language['text'];

        $model->updateViews();
        return view($this->getViewRoute('one'), [
            'model' => $model,
            'actual' => $article->getActual($model, $this->config->findBySlug('actual_news')),
            'lang' => ($lang) ? $lang->id : 1,
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);
    }

    public function mediaAction(Request $request)
    {
        $media = new Media();
        return view($this->getViewRoute('media'), [
            'collection' => $media->paginateMedia($this->config->findBySlug('paginate_media')),
            'menu' => $this->getMenu(),
            'sponsors' => $this->getSponsors()
        ]);

    }

    protected function translit($s)
    {
        $s = (string)$s;
        $s = strip_tags($s);
        $s = str_replace(array("\n", "\r"), " ", $s);
        $s = preg_replace("/\s+/", ' ', $s);
        $s = trim($s);
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
        $s = strtr($s, [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'j',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ы' => 'y',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'ъ' => '',
            'ь' => ''
        ]);
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
        $s = str_replace(" ", "-", $s);
        return $s;
    }

    public function redirect(Request $request, $provider)
    {
        if ($provider == 'vk') {
            $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/auth/vk/callback';
            $params = array(
                'client_id' => env('VK_CLIENT_ID'),
                'redirect_uri' => $redirect_uri,
                'response_type' => 'code'
            );
            return redirect(env('VK_OAUTH2_URL') . '?' . urldecode(http_build_query($params)));
        }
        if (!$provider == 'facebook') {
            $this->prefix = 'site';
            return redirect($this->getRoute());
        }
        return Socialite::driver($provider)->redirect();
    }

    public function authAction(Request $request, $provider)
    {
        $service = new User();

        $socialize = null;
        $user = [];
        if ($provider == 'facebook') {
            $socialize = Socialite::driver($provider)->user();
            if (!$socialize) {
                return abort(401);
            }
            $user['email'] = $socialize->email;
            $user['name'] = $socialize->name;
        } elseif ($provider == 'vk') {
            $vkData = $this->vkOAuth();
            if (empty($vkData)) {
                return abort(401);
            }
            $user['email'] = "{$vkData['screen_name']}_{$vkData['uid']}@vk.com";
            $user['name'] = "{$vkData['first_name']} {$vkData['last_name']}";
        }
        if (!empty($user)) {
            $userModel = $this->checkUser($service, $user['email'], $user['name']);
            $service->auth(['login' => $userModel->login, 'password' => '1']);
            if (!$userModel->token) {
                $userModel->createToken($userModel);
            }
            $request->session()->put('token', $userModel->token->getApiKey());
            return redirect($request->capture()->server('HTTP_REFERER'));
        }
        return abort(401);
    }

    public function logoutAction(Request $request)
    {
        $request->session()->forget('token');
        return redirect($request->capture()->server('HTTP_REFERER'));
    }

    private function checkUser(User $service, $email, $name)
    {
        $user = $service->getModel()->query()->where('email', $email)->orWhere('login', $this->translit($name))->first();
        if (!$user) {
            $login = explode(' ', $name);
            $user = $service->getModel()->create([
                'first_name' => $login[0],
                'last_name' => $login[1],
                'email' => $email,
                'login' => $this->translit($name),
                'password' => '1',
                'role_id' => 1,
                'status' => 1
            ]);
        }
        return $user;
    }

    private function vkOAuth()
    {
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/auth/vk/callback'; // Адрес сайта
        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => env('VK_CLIENT_ID'),
                'client_secret' => env('VK_PRIVATE_KEY'),
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
            );

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
            $userInfo = [];
            if (isset($token['access_token'])) {
                $params = array(
                    'uids' => $token['user_id'],
                    'fields' => 'uid,first_name,last_name,screen_name',
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['response'][0]['uid'])) {
                    $userInfo = $userInfo['response'][0];
                }
            }
            return $userInfo;
        }
        return [];
    }

    public function addCommentAction(Request $request, $id)
    {
        $this->setRules([
            '_user_id' => 'required|exists:users,id',
            'comment_text' => 'required',
        ]);
        $message = $request->input('comment_text');
        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        if (!$message) {
            return $this->sendWithErrors('Check your text for html tags');
        }
        $comment = new App\Modules\PieComments\Http\Services\Comments();
        $comment->getModel()->fill(
            [
                'user_id' => $request->input('_user_id'),
                'article_id' => $id,
                'comment_text' => $message,
            ]
        );
        $comment->getModel()->save();
        return $this->sendOk(['message' => 'Success', 'data' => $comment->getModel()->getInfo()]);
    }

    public function loadCommentAction(Request $request, $id)
    {
        $page = $request->input('page');
        $offset = $request->input('offset');
        $comment = new App\Modules\PieComments\Http\Services\Comments();
        $comment->setWhere(['article_id' => $id]);
        $count = $comment->getAll()->count();
        $collection = $comment->getAll($this->config->findBySlug('comments_count'), $offset);
        return $this->sendOk(['collection' => $collection->map(function ($model) {
            return $model->getInfo();
        }),
            'count' => $count,
            'limit' => $this->config->findBySlug('comments_count'),
            'page' => $page + 1
        ]);
    }

    public function loginAction(Request $request)
    {
        $service = new User();
        $user['email'] = $request->input('auth_name');
        $service->auth(['login' => $this->checkUser($service, $user['email'], $user['email'])->login,
            'password' => $request->input('auth_pass')]);
        if (!$service->isErrors()) {
            $request->session()->put('token', $service->getModel()->token->getApiKey());
        }
        return $this->sendOk(['goto' => $request->capture()->server('HTTP_REFERER')]);
    }

    public function slugAction(Request $request, $slug)
    {
        $page = new StaticPie();
        $model = $page->getPageBySlug($slug);
        if(!$model){
            abort(404);
        }
        $language = new Languages();
        $lang = $language->getModel()->where('name', App::getLocale())->first();
        return view($this->getViewRoute('slug'),[
            'content' => $model,
            'menu' => $this->getMenu(),
            'lang' => ($lang) ? $lang->id : 1,
            'sponsors' => $this->getSponsors()
        ]);
    }

}