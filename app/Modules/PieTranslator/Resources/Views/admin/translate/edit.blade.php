@extends('pie_base::admin.layouts.edit')

@section('title_name')Translate@endsection

@section('form_body')
    <div class="form-group">
        {!! Form::label('Language') !!}
        {!! Form::select('language', $lang,$model->language, ['class'=>'form-control']) !!}
    </div>
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    {!! Form::hidden('article_id', ($model->article_id)?:$article->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label(trans('pie_base::main.table_name')) !!}
        {!! Form::text('trans_name', ($model->trans_name)?:$article->name, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_info')) !!}
        {!! Form::text('trans_info', ($model->trans_info)?:$article->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_text')) !!}
        {!! Form::textarea('trans_text', ($model->trans_text)?:$article->text, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
    <div class="form-group">
        {!! Form::label(trans('pie_article::main.table_active')) !!}
        {!! Form::checkbox('status',true,($model->status == \App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE)) !!}
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/themes/gray.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor_ie8.min.js')}}"></script>
    <![endif]-->
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/tables.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/lists.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/media_manager.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_family.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_size.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/block_styles.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/video.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').editable({
                inlineMode: false,
                theme: 'gray',
                height: '300',
                language: 'ru'
            })
            $('.froala-box div').last().remove()
        });

    </script>
@endsection