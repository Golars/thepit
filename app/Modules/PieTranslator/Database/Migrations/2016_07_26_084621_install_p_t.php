<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\PieTranslator\Database\Models\LanguageModel;

class InstallPT extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name',2);
		});
		LanguageModel::create(
			[
				'id' => 1,
				'name' => 'en'
			]
		);
		LanguageModel::create(
			[
				'id' => 2,
				'name' => 'ru'
			]
		);
		LanguageModel::create(
			[
				'id' => 3,
				'name' => 'ua'
			]
		);
		Schema::create('translate_articles', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('article_id')->references('id')->on('articles');
			$table->integer('language')->default(1);
			$table->string('trans_name');
			$table->string('trans_info')->default('');
			$table->longText('trans_text');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('translate_articles');
	}
}
