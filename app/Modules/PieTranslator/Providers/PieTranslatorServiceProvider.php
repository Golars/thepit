<?php
namespace App\Modules\PieTranslator\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieTranslatorServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieTranslator module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieTranslator\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieTranslator module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_translator', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_translator', base_path('resources/views/vendor/pie_translator'));
		View::addNamespace('pie_translator', realpath(__DIR__.'/../Resources/Views'));
	}
}
