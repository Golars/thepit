<?php
namespace App\Modules\PieTranslator\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class Languages extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieTranslator\Database\Models\LanguageModel';
    protected $hasStatus = false;
    protected $orderBy = [];

    public function prepareSelect()
    {
        $langs = [];
        foreach ($this->getAll() as $lang) {
            $langs[$lang->id] = $lang->name;
        }
        return $langs;
    }
}