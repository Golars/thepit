<?php
namespace App\Modules\PieComments\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieComments\Http\Services\Censor;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 22.08.2016
 * Time: 17:51
 */
class Comments extends Base
{
    protected $with = [
        'user'
    ];

    public $timestamps = true;
    protected $table = 'comments';

    protected $fillable = array(
        'user_id',
        'article_id',
        'comment_text',
        'status',
    );

    public function user(){
        return $this->belongsTo('\App\Modules\PieBase\Database\Models\User','user_id');
    }

    public function getUser(){
        return (isset($this->user)) ? $this->user : App::make('\App\Modules\PieBase\Database\Models\User')->setDefault();
    }

    public function setCommentTextAttribute($value)
    {
        $this->attributes['comment_text'] = htmlspecialchars($value);
    }

    public function getMessage()
    {
        return nl2br(Censor::Replacement($this->attributes['comment_text']));
    }

    public function getInfo()
    {
        return [
            'text' => $this->getMessage(),
            'user' => [
                'name' => $this->getUser()->getFullName(),
                'avatar' => $this->getUser()->getAvatar(75)
            ],
            'created_at' => $this->created_at->setTimeZone('Europe/Kiev')
        ];
    }


}