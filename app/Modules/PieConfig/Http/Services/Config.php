<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 04.09.2016
 * Time: 21:24
 */

namespace App\Modules\PieConfig\Http\Services;


use App\Modules\PieBase\Http\Services\Base;

class Config extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieConfig\Database\Models\Config';
    protected $hasStatus = false;

    /**
     * @param $slug
     * @return null
     */
    public function findBySlug($slug)
    {
        $model = $this->getModel()->query()->where('slug',$slug)->get()->first();
        if(!$model){
            return null;
        }
        return $model->value;
    }
}