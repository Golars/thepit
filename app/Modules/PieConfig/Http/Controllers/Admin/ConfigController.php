<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 04.09.2016
 * Time: 22:11
 */

namespace App\Modules\PieConfig\Http\Controllers\Admin;

use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    protected $prefix = 'admin:config';
    protected $moduleName = 'pie_config';
    protected $serviceName = 'App\Modules\PieConfig\Http\Services\Config';

    public function edit(Request $request, $id){
        $this->setRules([
            'id'       =>  'required',
            'name'     =>  'required|min:2',
            'value'    =>  'required',
        ]);
        return parent::edit($request, $id);
    }

}