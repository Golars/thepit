<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 21.07.2016
 * Time: 15:41
 */

namespace App\Modules\PieStatic\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;
use App\Modules\PieBase\Http\Requests\Response;

class StaticTranslateController extends Controller
{
    protected $prefix = 'admin:static:translate';
    protected $moduleName = 'pie_static';
    protected $serviceName = 'App\Modules\PieStatic\Http\Services\StaticPieTranslate';

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'lang' => App::make('App\Modules\PieTranslator\Http\Services\Languages')->prepareSelect(),
        ];
    }

    public function index()
    {
        return redirect(route('admin:static:index'));
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'title' => 'required|min:2',
            'slug' => 'required|min:2',
            'info' => 'required|min:3',
            'content' => 'required|min:5',
            'display_image' => '',
            'in_menu' => '',
            'position' => 'required|numeric',
            'file_id' => '',
        ]);

        if ($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('file_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    public function addTranslate(Request $request, $id)
    {
        $service = new App\Modules\PieStatic\Http\Services\StaticPie();
        return view($this->getViewRoute('edit'),
            $this->prepareData() + ['page' => $service->getOne($id)]  );
    }

    public function saveTranslate(Request $request)
    {
        $this->setRules([
            'static_page_id' => 'required',
            'language' => 'required',
            'title_translate' => 'required|min:2',
            'info_translate' => 'required|min:3',
            'content_translate' => 'required|min:5',
            'status' => ''
        ]);

        if ($this->service->getModel()
            ->where('static_page_id', $request->input('static_page_id'))
            ->where('language', $request->input('language'))
            ->first()
        ) {
            return $this->sendWithErrors(['This language already added']);
        }
        return parent::add($request);
    }

    public function editTranslate(Request $request,$id)
    {
        $this->setRules([
            'id' => 'required',
            'static_page_id' => 'required',
            'language' => 'required',
            'title_translate' => 'required|min:2',
            'info_translate' => 'required|min:3',
            'content_translate' => 'required|min:5',
            'status' => ''
        ]);
        return parent::edit($request, $id);
    }
}