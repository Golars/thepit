<?php
namespace App\Modules\PieStatic\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class StaticPie extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieStatic\Database\Models\PiePage';

    public function getPageBySlug($slug)
    {
        return $this->getModel()->active()->where('slug',$slug)->first();
    }

    public function prepareSelect()
    {
        $pages = [];
        foreach ($this->getAll() as $page) {
            $pages[$page->id] = $page->title;
        }
        return $pages;
    }
    
    public function getMenu()
    {
        return $this->getModel()->menu()->active()->orderBy('position')->get();
    }
}