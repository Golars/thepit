<?php
namespace App\Modules\PieStatic\Http\Services;

use App\Modules\PieBase\Http\Services\Files;

class Image extends Files
{
    protected $modelName = 'App\Modules\PieStatic\Database\Models\Cover';
    protected $orderBy = [];
    protected $hasStatus = false;
}