<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 21.07.2016
 * Time: 15:30
 */

namespace App\Modules\PieStatic\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieTranslator\Database\Models\LanguageModel;
use App\Modules\PieStatic\Database\Models\PiePage;

class PiePageTranslate extends Base
{
    public $timestamps = true;
    protected $table = 'static_pages_translate';

    protected $fillable = [
        'static_page_id',
        'title_translate',
        'info_translate',
        'content_translate',
        'language',
        'status'
    ];

    public function lang()
    {
        return $this->belongsTo(LanguageModel::class, 'language');
    }

    public function translate()
    {
        return $this->belongsTo(PiePage::class, 'static_page_id');
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = isset($value);
    }

}