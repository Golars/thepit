<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslate extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('static_pages_translate', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('static_page_id')->references('id')->on('static_pages');
			$table->text('content_translate');
			$table->string('info_translate');
			$table->string('title_translate');
            $table->integer('language')->default(1);
			$table->tinyInteger('status')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pie_static_translate');
	}
}
