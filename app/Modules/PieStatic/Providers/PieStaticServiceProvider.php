<?php
namespace App\Modules\PieStatic\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieStaticServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieStatic module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieStatic\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieStatic module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_static', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_static', base_path('resources/views/vendor/pie_static'));
		View::addNamespace('pie_static', realpath(__DIR__.'/../Resources/Views'));
	}
}
