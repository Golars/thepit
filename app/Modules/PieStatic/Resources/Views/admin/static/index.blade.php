@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pie_static::main.menu_static')}} @endsection

@section('tHead')
    <th>{{trans('pie_base::main.table_name')}}</th>
    <th>{{trans('pie_article::main.table_info')}}</th>
    <th>{{trans('pie_article::main.table_cover')}}</th>
    <th>In Menu</th>
    <th>Position</th>
    <th>slug</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->title}}

            </td>
            <td>{{$model->info}}</td>
            <td>
                <img src="{{$model->getCover()}}" width="75" class="img-responsive">
            </td>
            <td>
                @if($model->in_menu)
                    <span class="label label-success">
                        <i class="fa fa-plus-circle"></i>
                    </span>
                @else
                    <span class="label label-danger">
                        <i class="fa fa-minus-circle"></i>
                    </span>
                @endif
            </td>
            <td>
                {{$model->position}}
            </td>
            <td><a href="{{route('static:page',['slug'=>$model->slug])}}" target="_blank"
                   class="label label-info">{{$model->slug}}</a></td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:static:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                @if($model->translate && $model->translate->first())
                    @foreach($model->translate as $lang)
                        <a class="btn {{($lang->status == 1)?"btn-info":"btn-danger"}}"
                           href="{{route('admin:static:translate:edit',['id'=>$lang->id])}}">
                            <i class="fa fa-language"></i> {{$lang->lang->name}}
                        </a>
                    @endforeach
                @endif
                @if(!$model->translate || $model->translate->count() < 3)
                    <a class="btn btn-info" href="{{route('admin:static:translate:add',['id'=>$model->id])}}">
                        <i class="fa fa-plus"></i> <i class="fa fa-language"></i>
                    </a>
                @endif

                <a class="btn btn-success" href="{{route('admin:static:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:static:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection