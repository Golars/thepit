<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" Content="ru">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
            Submit
        @show
    </title>

    <!-- Bootstrap core CSS -->
    <link href="{{$app['pie_base.assets']->getPath('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{$app['pie_base.assets']->getPath('/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{$app['pie_base.assets']->getPath('/css/animate.min.css')}}" rel="stylesheet">
    <script src="{{$app['pie_base.assets']->getPath('/js/lib/jquery/jquery-2.1.3.min.js')}}"></script>
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet"
          type="text/css">
    <script type="text/javascript" src="{{$app['pie_base.assets']->getPath('/js/lib/underscore/underscore-min.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="favicon.ico"/>
    <link rel="icon" href="/favicon-32.png" sizes="32x32"/>
    <link rel="icon" href="/favicon-64.png" sizes="64x64"/>
    <!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->
    <!-- IE10 -->
    <meta name="msapplication-TileColor" content="#D83434">
    <meta name="msapplication-TileImage" content="/favicon.ico">

</head>
<body>
@yield('content')
@yield('scripts')
<script type="text/javascript"
        src="{{$app['pie_base.assets']->getPath('/js/lib/notify/pnotify.core.js')}}"></script>
<script>
    $('form').on('submit', function (e) {
        var form = $(e.target);
        if (!form.hasClass('popup')) {
            console.log(form);
            return true;
        }
        var formData = new FormData();
        form.serializeArray().reduce(function (obj, item) {
            formData.append(item.name, item.value);
            return obj;
        }, {});
        _.each(form.find('input[type=file]'), function (file) {
            if (file.files[0] != undefined) {
                console.log($(file).attr('name'), file.files[0]);
                formData.append($(file).attr('name'), file.files[0]);
            }
        });

        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (res) {
                var response = JSON.parse(res)
                new PNotify({
                    title: 'Успех',
                    text: 'Сохранение данных прошло успешно!',
                    type: 'success'
                })

                if (response.goto != undefined) {
                    console.log(1);
                    setTimeout(function () {
                        window.location.href = response.goto;
                    }, 1000);
                }
            },
            error: function (res) {
                console.log(res)
                var errors = JSON.parse(res.responseText).errors || ['Данные не валидные'];
                new PNotify({
                    title: 'Ошибка',
                    text: errors.join("<br>"),
                    type: 'error'
                });
                return false;
            }
        });

        return false;
    })
</script>
</body>
</html>

