<?php
$langService = (isset($_SERVER['HTTP_HOST'])) ? substr($_SERVER['HTTP_HOST'], 0, 2) : 'en';
if ($langService != 'ru') {
    $langService = '';
} else {
    $langService = 'ru.';
}
return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' =>
        [
            'client_id' => '282148912145666', // App ID
            'client_secret' => '74cacce2ffaace680c6c19c32d6b035d', // App Secret
            'redirect' => "http://{$langService}pie.sticky.loc/auth/facebook/callback", //Ссылка на перенаправление при удачной авторизации (3)
        ]
];
